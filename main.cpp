
#include <complex>
#include <iostream>
#include <functional>
#include <execution>
#include <chrono>
#include <filesystem>

#include "cimg/CImg.h"
#include "httplib/httplib.h"

using namespace std;
namespace fs = std::filesystem;
using namespace chrono;
using namespace cimg_library;
using namespace httplib;

struct Pixel {
    uint16_t x;
    uint16_t y;
    double convergencePoint;
};

struct MandelbrotPoint {
    complex<double> z;
    uint16_t x;
    uint16_t y;
};

struct CSetChunk {
    uint16_t x;
    uint16_t y;
    uint16_t w;
    uint16_t h;
};

vector<MandelbrotPoint> generateCSet(uint16_t width, uint16_t height, double setMinI, double setMaxI, double setMinR
                              , double setMaxR, int setSize) {

    cout << "Generating c set ..." << endl;
    auto start = high_resolution_clock::now();

    auto cSet = vector<MandelbrotPoint>();
    double pixelWidth = (setMaxR - setMinR)/(width - 1.0);
    double pixelHeight = (setMaxI - setMinI)/(height - 1.0);

    cout << "\twidth:\t\t" << width << endl << "\theight:\t\t" << height << endl <<
            "\tpixel width:\t" << pixelWidth << endl << "\tpixel height:\t" << pixelHeight << endl;

    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            MandelbrotPoint p;
            p.x = x;
            p.y = y;
            p.z = complex<double>(setMinR + x*pixelWidth, setMaxI - y*pixelHeight);
            cSet.push_back(p);
        }
    }

    auto compTime = duration_cast<milliseconds>(high_resolution_clock::now() - start);
    cout << "c set generated in " << compTime.count() << "ms." << endl;

    return cSet;
}

/*vector<MandelbrotPoint> generateCSetChunk(CSetChunk chunk, double setMinR, double setMaxI, double pixelWidth
        , double pixelHeight) {

    cout << "Generating c set ..." << endl;
    high_resolution_clock::time_point start = high_resolution_clock::now();

    auto cSetChunk = vector<MandelbrotPoint>();
    for (uint16_t x = chunk.x; x < chunk.x+chunk.w; x++) {
        for (uint16_t y = chunk.y; y < chunk.y+chunk.h; y++) {
            cSetChunk.push_back(MandelbrotPoint {
                    complex<double>(setMinR + x*pixelWidth, setMaxI - y*pixelHeight),
                    x, y
            });
        }
    }

    duration<double> compTime = duration_cast<milliseconds>(high_resolution_clock::now() - start);
    cout << "c set generated in " << compTime.count() << "ms." << endl;

    return cSetChunk;
}

vector<MandelbrotPoint> generateCSetP(uint16_t width, uint16_t height, double setMinI, double setMaxI, double setMinR
        , double setMaxR, int setSize) {

    cout << "Generating c set ..." << endl;
    high_resolution_clock::time_point start = high_resolution_clock::now();

    auto cSet = vector<MandelbrotPoint>();
    double pixelWidth = (setMaxR - setMinR)/(width - 1.0);
    double pixelHeight = (setMaxI - setMinI)/(height - 1.0);
    uint16_t chunkWidth = height / 2;
    uint16_t chunkHeight = width / 2;

    vector<CSetChunk> cSetChunks {
        CSetChunk{ 0, 0, chunkWidth, chunkHeight },
        CSetChunk{ static_cast<uint16_t>(width/2-1), 0, chunkWidth, chunkHeight },
        CSetChunk{ 0, static_cast<uint16_t>((height/2-1)), chunkWidth, chunkHeight },
        CSetChunk{ static_cast<uint16_t>(width/2-1), static_cast<uint16_t>(height/2-1), chunkWidth, chunkHeight }
    };

    transform_reduce(
            execution::par,
            cSetChunks.begin(), cSetChunks.end(),
            cSet.begin(),
            generateCSetChunk,
            [](vector<MandelbrotPoint> c1, vector<MandelbrotPoint> c2) {
                return c1.insert(c1.end(), c2.begin(), c2.end());
            });

    duration<double> compTime = duration_cast<milliseconds>(high_resolution_clock::now() - start);
    cout << "c set generated in " << compTime.count() << "ms." << endl;

    return cSet;
}*/

double computeConvergencePoint(complex<double> c, uint16_t maxI) {
    complex<double> zn(0.0, 0.0);

    for (double it = 0; it < maxI; it++) {
        double r2 = zn.real()*zn.real();
        double i2 = zn.imag()*zn.imag();

        if(r2 + i2 > 4.0) return it;

        zn.imag(2.0 * zn.real() * zn.imag() + c.imag());
        zn.real(r2 - i2 + c.real());
    }

    return maxI;
}

void paint(Pixel p, CImg<double> view, double colorScaling) {
    double c = colorScaling * p.convergencePoint;
    view(p.x, p.y, 0, 0) = c;
    view(p.x, p.y, 0, 1) = c;
    view(p.x, p.y, 0, 2) = c;
}

CImg<uint8_t> renderMandelbrotSet(uint16_t width, uint16_t height, double minR, double maxR, double minI,
                         double maxI, uint16_t maxIt) {
    //auto start = high_resolution_clock::now();
    int setSize = width * height;

    cout << "problem size:\t" << setSize << endl;

    vector<MandelbrotPoint> cSet = generateCSet(width, height, minI, maxI, minR, maxR, setSize);

    vector<Pixel> set = vector<Pixel>(setSize);
    transform(execution::par_unseq, cSet.begin(), cSet.end(), set.begin(), [maxIt](MandelbrotPoint p) {
        return Pixel { p.x, p.y, computeConvergencePoint(p.z, maxIt) };
    });

    double colorScaling = 255.0 / maxI;
    CImg<double> view = CImg(width, height, 1, 3, 0);
    for (int i = 0; i < setSize; i++) {
        Pixel p = set[i];
        double c = colorScaling * p.convergencePoint;
        view(p.x, p.y, 0, 0) = c;
        view(p.x, p.y, 0, 1) = c;
        view(p.x, p.y, 0, 2) = c;
    }

    /*auto end = high_resolution_clock::now();
    duration<double> diff = end-start;
    cout << "Mandelbrot set computation took " << diff.count() << "s." << endl;*/

    cSet.clear();
    set.clear();

    return view;
}

int main() {
    Server httpServer;
    SocketOptions options;
    httpServer.set_mount_point("/", filesystem::current_path().string().c_str());
    httpServer.set_file_extension_and_mimetype_mapping("bmp", "image/bmp");

    httpServer.Get("/mandelbrot", [](const httplib::Request &req, httplib::Response &res) {
        if(req.params.contains("width") && req.params.contains("height")
            && req.params.contains("maxIt") && req.params.contains("minI")
            && req.params.contains("maxI") && req.params.contains("minR")
            && req.params.contains("maxR")) {

            uint16_t width = stoi(req.params.find("width")->second);
            uint16_t height = stoi(req.params.find("height")->second);
            uint16_t maxIt = stoi(req.params.find("maxIt")->second);
            double minI = stod(req.params.find("minI")->second);
            double maxI = stod(req.params.find("maxI")->second);
            double minR = stod(req.params.find("minR")->second);
            double maxR = stod(req.params.find("maxR")->second);

            cout << "Rendering Mandelbrot set ..." << endl
                 << "frame:\t[" << minR << "," << minI << "," << maxR << "," << maxI << "]" << endl
                 << "output:\t[" << width << "," << height << "]" << endl
                 << "maxIt:\t" << maxIt << endl << endl;

            CImg<uint8_t> renderResult = renderMandelbrotSet(width, height, minR, maxR, minI, maxI, maxIt);
            string fileName = "render-" + to_string(std::time(nullptr)) + ".bmp";
            string fullPath = filesystem::current_path().string() + "\\" + fileName;
            renderResult.save_bmp(fullPath.c_str());

            cout << fullPath << endl;

            res.set_header("Access-Control-Allow-Origin", "*");
            res.set_content(fileName, "text/plain");
        }
    });

    httpServer.listen("localhost", 8080);

    return 0;
}
