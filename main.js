var minI = -1.0;
var minR = -2.0;
var maxI = 1.0;
var maxR = 1.0;
var pixelWidth;
var pixelHeight;
let maxIt = 250;
let zoomRatio = 0.5;

$(() => {
    init();
    $("img").on("click", function(event) {
        let x = event.pageX - this.offsetLeft;
        let y = event.pageY - this.offsetTop;
        zoom(x, y);
    });
});

function zoom(x, y) {
    //alert("(" + x + ", " + y + ")");
    pixelWidth = (maxR - minR)/(screen.availWidth - 1.0);
    pixelHeight = (maxI - minI)/(screen.availHeight - 1.0);
    let focusR = x*pixelWidth + minR;
    let focusI = y*pixelHeight + minI;

    //alert("focus point: (" + focusR + ", " + focusI + ")");

    let minR_ = focusR - minR*zoomRatio;
    let maxR_ = focusR + maxR*zoomRatio;
    let minI_ = focusI - minI*zoomRatio;
    let maxI_ = focusI + maxI*zoomRatio;

    //alert(minR_ + "," + minI_ + "," + maxR_ + "," + maxI_);

    minI = minI_;
    minR = minR_;
    maxI = maxI_;
    maxR = maxR_;
    render(minI, maxI, minR, maxR, maxIt);
}

function generateQuery(minI, maxI, minR, maxR, maxIt) {
    return "http://localhost:8080/mandelbrot?width=" + screen.availWidth + "&height=" + screen.availHeight + "&maxIt="
        + maxIt + "&minI=" + minI + "&minR=" + minR + "&maxI=" + maxI + "&maxR=" + maxR;
}

function render(minI, maxI, minR, maxR, maxIt) {
    $.get(generateQuery(minI, maxI, minR, maxR, maxIt), function(data){
        $(".clickable").attr("src", "http://localhost:8080/" + data);
    });
}

function init() {
    render(minI, maxI, minR, maxR, maxIt);
}
